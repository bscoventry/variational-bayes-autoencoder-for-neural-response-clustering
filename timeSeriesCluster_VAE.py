import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pdb
from VAE import Encoder,Decoder,variationalAutoEncoder
from utils import timeSeriesDataLoader,plot_clustering
import pickle
from sklearn.cluster import KMeans
from torch.utils.data import DataLoader, TensorDataset
import tensorflow as tf
import datetime
from scipy.io import savemat
if __name__ == "__main__":
    if torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'

    #torch.backends.cudnn.enabled = False

    #Hyperparameters
    hidden_size = 90
    num_hidden = 1
    latent_length = 20
    batch_size = 4
    learning_rate = 0.0005
    n_epochs = 2000
    dropout_rate = 0.2
    cuda = True # options: True, False
    print_every=30
    clip = True # options: True, False
    max_grad_norm=1
    #device = get_device()
    dtype = torch.cuda.FloatTensor
    if torch.cuda.is_available():
        device = torch.device("cuda:0")  # you can continue going on here, like cuda:1 cuda:2....etc. 
        print("Running on the GPU")
    else:
        device = torch.device("cpu")
        print("Running on the CPU")
    #Load data
    df_timeSeries = pd.read_csv('BarsVRAE.csv')
    timeSeries = timeSeriesDataLoader(df_timeSeries,torch.cuda.FloatTensor)
    #timeSeriesData = DataLoader(timeSeries,batch_size = batch_size,shuffle = True)
    #Do the model baby!
    sequence_size = 12
    num_inputs = 1
    hidden_size = 200
    dropout = 0.2 
    batch_size = 4
    learning_rate = 0.0005
    num_epochs = 1000
    num_hidden = 5
    latent_size = 25
    clip = True
    saveState = False
    vAe = variationalAutoEncoder(num_inputs=num_inputs,num_hidden=num_hidden,hidden_size=hidden_size,latent_size=latent_size,sequence_size=sequence_size,batch_size = batch_size,learning_rate=learning_rate,dropout=dropout,num_epochs=num_epochs,save=saveState)
    if torch.cuda.device_count() > 1:
        vAe = torch.nn.DataParallel(vAe)
    pdb.set_trace()
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    vAe.fit(timeSeries)
    z_run = vAe.forward_cluster(timeSeries)
    z_Cluster = KMeans(n_clusters = 2, random_state=0).fit(z_run)
    labels = z_Cluster.labels_
    plot_clustering(z_run,labels, engine ='matplotlib', download = False)
    pickle.dump(labels, open( "labels.p", "wb" ) )
    pickle.dump(z_run, open( "z_run.p", "wb" ) )
    torch.save(vAe, 'vAe.pth')
    savemat('labels.mat',{'mylabels': labels})
    pdb.set_trace()
