# README #

Recurrent Variational Autoencoders (VRAE) for Neural Response Clustering

### What is this repository for? ###

Implemented within is a recurrent variational autoencoder suited for clustering of time-series neural data. 
Version: 1.0


### How do I get set up? ###


* Dependencies:
* PyTorch
* Numpy
* Matplotlib/Plotly
* PANDAS
* Nvidia GPU/Cuda


### Who do I talk to? ###

Owner and Contact: Brandon S Coventry
Email: bscoventry@gmail.com