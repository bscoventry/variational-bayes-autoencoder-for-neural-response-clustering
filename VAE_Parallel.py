import numpy as np
import torch
import os
import numpy as np
from utils import timeSeriesDataLoader
import torch.nn.functional as F
import pdb

class Encoder(torch.nn.Module):
    """Class Encoder architecure.
    Inputs:
        num_inputs: number of input features
        num_hidden: number of hidden layers
        hidden_size: number of features to hidden layer.
        latent_size: 
    Outputs:
    """
    def __init__(self,num_inputs,num_hidden,hidden_size,latent_size,dropout):
        super(Encoder,self).__init__()
        self.num_inputs = num_inputs
        self.num_hidden = num_hidden
        self.hidden_size = hidden_size
        self.dropout = dropout
        self.latent_size = latent_size
        self.LTSM = torch.nn.LSTM(self.num_inputs,self.hidden_size,self.num_hidden)
        self.linearLambdaMean = torch.nn.Linear(self.hidden_size,self.latent_size)
        self.linearLambdaVar = torch.nn.Linear(self.hidden_size,self.latent_size)
        torch.nn.init.xavier_uniform_(self.linearLambdaMean.weight)
        torch.nn.init.xavier_uniform_(self.linearLambdaVar.weight)
    def forward(self,x):
        outputState, (h_n,c_n) = self.LTSM(x)
        h_n = h_n[-1,:,:]          #Reshape h_n
        self.lambdaMean = self.linearLambdaMean(h_n)
        self.lambdaVar = self.linearLambdaMean(h_n)
        if self.training:
            std = torch.exp(0.5 * self.lambdaVar)
            eps = torch.randn_like(std)
            return eps.mul(std).add_(self.lambdaMean)
        else:
            return torch.nn.Softmax(self.latent_mean)

    
class Decoder(torch.nn.Module):
    """Class Decoder Architecture

    """
    def __init__(self,num_inputs,num_hidden,hidden_size,latent_size,sequence_size,batch_size,output_size,dropout):
        super(Decoder,self).__init__()
        self.num_inputs = num_inputs
        self.num_hidden = num_hidden
        self.hidden_size = hidden_size
        self.latent_size = latent_size
        self.sequence_size = sequence_size
        self.batch_size = batch_size
        self.output_size = output_size
        self.dropout = dropout
        self.dtype = torch.cuda.FloatTensor
        self.LTSM = torch.nn.LSTM(1,self.hidden_size,self.num_hidden)
        self.latent2Hidden = torch.nn.Linear(self.latent_size,self.hidden_size)
        self.hidden2Output = torch.nn.Linear(self.hidden_size,self.output_size)
        torch.nn.init.xavier_uniform(self.latent2Hidden.weight)
        torch.nn.init.xavier_uniform(self.hidden2Output.weight)

        self.decoder_Inputs = torch.zeros(self.sequence_size,self.batch_size,1,requires_grad=True).type(self.dtype)
        self.c_0 = torch.zeros(self.num_hidden,self.batch_size,self.hidden_size,requires_grad=True)
        self.c_0 = self.c_0.to(device='cuda')
    def forward(self,x):
        cur_h_state = self.latent2Hidden(x)
        h_0 = torch.stack([cur_h_state for ck in range(self.num_hidden)])
        h_0 = h_0.to(device='cuda')
        
        decoderOut, _ = self.LTSM(self.decoder_Inputs,(h_0,self.c_0))
        output = self.hidden2Output(decoderOut)
        return output

class variationalAutoEncoder(torch.nn.Module):
    def __init__(self,num_inputs,num_hidden,hidden_size,latent_size,sequence_size,batch_size,learning_rate,dropout,num_epochs):
        super(variationalAutoEncoder,self).__init__()
        self.num_inputs = num_inputs
        self.num_hidden = num_hidden
        self.hidden_size = hidden_size
        self.latent_size = latent_size
        self.sequence_size = sequence_size
        self.batch_size = batch_size
        #self.output_size = output_size
        self.dropout = dropout
        self.dtype = torch.cuda.FloatTensor
        self.learning_rate = learning_rate
        self.Encoder = Encoder(self.num_inputs,self.num_hidden,self.hidden_size,self.latent_size,self.dropout)
        self.Encoder.cuda()
        self.Decoder = Decoder(self.num_inputs,self.num_hidden,self.hidden_size,self.latent_size,self.sequence_size,self.batch_size,self.num_inputs,self.dropout)
        self.Decoder.cuda()
        self.loss_fn = torch.nn.MSELoss(reduction='sum')
        self.optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate)
        self.num_epochs = num_epochs
        self.clipping = True
        self.num_epochs = num_epochs
        self.max_grad_norm = 5
        self.dload = './model_dir'
    def forward(self,x):
        encoder_out = self.Encoder(x)
        
        decoder_out = self.Decoder(encoder_out)
        return decoder_out,encoder_out
    
    def calcKLLoss(self,decoder_out,x):
        latent_mean, latent_logvar = self.Encoder.lambdaMean, self.Encoder.lambdaVar
        kl_loss = -0.5 * torch.mean(1 + latent_logvar - latent_mean.pow(2) - latent_logvar.exp())
        recon_loss = self.loss_fn(decoder_out, x)
        return kl_loss + recon_loss, recon_loss, kl_loss
    
    def calculateLoss(self,x):
        
        transformed_x = torch.autograd.Variable(x[:,:,:].type(self.dtype), requires_grad = True)
        enc = self.Encoder(transformed_x)
        x_decoded = self.Decoder(enc)
        #x_decoded, _ = self(transformed_x)
        totloss,recon_loss,kl_loss = self.calcKLLoss(x_decoded,transformed_x.detach())
        return totloss,recon_loss,kl_loss
    
    def train(self,dataloader):
        epoch_loss = 0
        ck = 0
        for ck,data in enumerate(dataloader):
            
            #data = data[0]   #Returns tensor
            # required to swap axes, since dataloader gives output in (batch_size x seq_len x num_of_features)
            
            data = data.permute(1,0,2)
            #data = torch.squeeze(data)
            self.optimizer.zero_grad()        #Set gradients to 0
            
            loss, reconstruction_loss,kl_loss = self.calculateLoss(data)
            loss.backward()
            if self.clipping:
                torch.nn.utils.clip_grad_norm_(self.parameters(), max_norm = self.max_grad_norm)
            epoch_loss = epoch_loss + loss.item()
            self.optimizer.step()

        print('Average loss: {:.4f}'.format(epoch_loss / ck))
    
    def fit(self,data,save=True):
        dataset = torch.utils.data.DataLoader(dataset=data,batch_size = self.batch_size,shuffle = True,drop_last = True)
        
        for jk in range(self.num_epochs):
            print('Epoch: '+str(jk))
            self.train(dataset)

        self.fitComplete = True
        if save:
            self.save('model.pth')
    
    def forward_cluster(self,data,save=False):
        """This function clusters the data after training
        """
        
        data = torch.utils.data.DataLoader(dataset=data,batch_size = self.batch_size,shuffle=False,drop_last = True)
        if self.fitComplete:
            with torch.no_grad():
                run_accumulator = []
                for ck,x in enumerate(data):
                    x = x.permute(1, 0, 2)
                    currun = self.Encoder(torch.autograd.Variable(x.type(self.dtype), requires_grad = False)).cpu().data.numpy()
                    run_accumulator.append(currun)
                run_accumulator = np.concatenate(run_accumulator,axis = 0)
                return run_accumulator
        else:
            print('Model needs to be fit first')
        
    def save(self,filename):
        """
        Pickles the model parameters to be retrieved later
        :param file_name: the filename to be saved as,`dload` serves as the download directory
        :return: None
        """
        PATH = self.dload + '/' + filename
        if os.path.exists(self.dload):
            pass
        else:
            os.mkdir(self.dload)
        torch.save(self.state_dict(), PATH)




    