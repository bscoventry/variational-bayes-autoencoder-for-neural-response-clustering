import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pdb
from VAE import Encoder,Decoder,variationalAutoEncoder
from utils import timeSeriesDataLoader,plot_clustering
from sklearn.cluster import KMeans
from torch.utils.data import DataLoader, TensorDataset
if __name__ == "__main__":
    if torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'

    #Hyperparameters
    hidden_size = 90
    num_hidden = 1
    latent_length = 20
    batch_size = 4
    learning_rate = 0.0005
    n_epochs = 2000
    dropout_rate = 0.2
    cuda = True # options: True, False
    print_every=30
    clip = True # options: True, False
    max_grad_norm=1
    #device = get_device()
    df_timeSeries = pd.read_csv('BarsVRAE.csv')
    timeSeries = timeSeriesDataLoader(df_timeSeries,torch.cuda.FloatTensor)
    #timeSeriesData = DataLoader(timeSeries,batch_size = batch_size,shuffle = True)
    #Do the model baby!
    sequence_size = 12
    num_inputs = 1
    hidden_size = 200
    dropout = 0.2 
    batch_size = 8
    learning_rate = 0.0005
    num_epochs = 3000
    num_hidden = 5
    latent_size = 25

    #Load model
    vAeRecon = torch.load('vAe.pth')
    pdb.set_trace()
    vAeRecon.eval()
    z_run = vAeRecon.forward_cluster(timeSeries)
    z_Cluster = KMeans(n_clusters = 2, random_state=0).fit(z_run)
    labels = z_Cluster.labels_
    pdb.set_trace()